#include <stddef.h>
#include <string.h>

#include "leventos.h"
#include "macros.h"

typedef struct node {
    Leventos_Event_t event;
    struct node *next;
}node_t;

static bool delay;
static Leventos_EventHardwareInterface_t localInterface;

#define MAX_EVENTS 20
static node_t events[MAX_EVENTS];
static node_t *head = NULL;

static int node_add(Leventos_Event_t *event);
static int node_remove(void (*funct)(void), Leventos_EventRemove_t remove);
static node_t *node_search_free(void);

static void attach_to_execute(void);
static void delay_complite(void);
static void execute(void);
static void substract_time_from_all(uint32_t cnt);

static void overlap(void);
static uint64_t getTick(Leventos_Time_t time);

volatile uint32_t IRQs;
uint8_t max = 0;

void Leventos_init(Leventos_EventHardwareInterface_t interface)
{
    localInterface = interface;
}

void Leventos_delay(Leventos_Time_t time, void (*sleep)(void))
{
    Leventos_add(time, delay_complite);
    while(!delay)
    {
        if(sleep)
        {
            sleep();
            Leventos_handler();
        }
        else
        {
            Leventos_handler();
        }
    }
    delay = false;
}

int Leventos_add(Leventos_Time_t time, void (*funct)(void))
{
    if(funct == NULL)
    {
        return 0;
    }

    substract_time_from_all(localInterface.getTimerCounter());
    int result = node_add(&(Leventos_Event_t){.funct = funct, .time = getTick(time)});
    attach_to_execute();
    return result;
}

int Leventos_delete(void (*funct)(void), Leventos_EventRemove_t remove)
{
    int result = node_remove(funct, remove);
    substract_time_from_all(localInterface.getTimerCounter());
    attach_to_execute();
    return result;
}

void Leventos_handler(void)
{
    while(IRQs)
    {
        IRQs = 0;
        execute();
    }
}

void Leventos_irq(void)
{
    IRQs = 1;
}

static void execute(void)
{
    if(head == NULL)
    {
        return;
    }
    void (*funct[MAX_EVENTS])(void);
    int exeCounter = 0;

    substract_time_from_all(localInterface.getTimerCounter());
    while(head != NULL && head->event.time <= 0)
    {
        funct[exeCounter] = head->event.funct;
        ++exeCounter;
        node_remove(head->event.funct, Leventos_EventRemoveOne);
    }
    attach_to_execute();
    for(int i = 0; i < exeCounter; ++i)
    {
        RUN(funct[i]);
    }
}

static void attach_to_execute(void)
{
    node_t *current = head;
    while(current != NULL)
    {
        if(current->event.time > 0)
        {
        	int overlapTick = localInterface.getOverlapTick();
        	if(current->event.time > overlapTick)
        	{
        		node_add(&(Leventos_Event_t){.funct = overlap, .time = overlapTick});
        		localInterface.setNewTime(overlapTick);
        	}
        	else
        	{
        		localInterface.setNewTime(current->event.time);
        	}
            break;
        }
        current = current->next;
    }
}

static void substract_time_from_all(uint32_t cnt)
{
    node_t *current = head;
    while(current != NULL)
    {
        current->event.time -= cnt;
        if(current->event.time <= 0)
        {
            IRQs = 1;
        }
        current = current->next;
    }

    localInterface.turnOffTimer();
}

static int node_add(Leventos_Event_t *event)
{
    node_t *new;
    if(NULL == (new = node_search_free()))
    {
        return 0;
    }
    memcpy(&new->event, event, sizeof(Leventos_Event_t));
    if(NULL == head || head->event.time >= event->time)
    {
        new->next = head;
        head = new;
    }
    else
    {
        node_t *current = head;
        while(current->next != NULL && current->next->event.time <= event->time)
        {
            current = current->next;
        }
        new->next = current->next;
        current->next = new;
    }
    return 1;
}

static int node_remove(void (*funct)(void), Leventos_EventRemove_t remove)
{
    int counter = 0;
    while(head && head->event.funct == funct)
    {
        head->event.funct = NULL;
        head = head->next;
        counter += 1;
        if(Leventos_EventRemoveOne == remove)
        {
            return counter;
        }
    }
    node_t *current = head;
    while(current && current->next)
    {
        if(current->next->event.funct == funct)
        {
            current->next->event.funct = NULL;
            current->next = current->next->next;
            counter += 1;
            if(Leventos_EventRemoveOne == remove)
            {
                return counter;
            }
        }
        current = current->next;
    }
    return counter;
}

static node_t *node_search_free(void)
{
    for(int i = 0; i < MAX_EVENTS; ++i)
    {
        if(events[i].event.funct == NULL)
        {
            if(i > max)
            {
                max = i;
            }
            return &events[i];
        }
    }
    return NULL;
}

static void delay_complite(void)
{
    delay = true;
}

static void overlap(void)
{
	PASS();
}

static uint64_t getTick(Leventos_Time_t time)
{
	float divider = localInterface.getUsInOneTick();
	uint64_t tick = ((time.us + (time.ms * 1000) + (time.s * 1000000)) / divider);
	uint32_t correction = (tick / localInterface.getOverlapTick()) * 32;
	return tick - correction;
}
