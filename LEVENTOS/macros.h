#ifndef MACROS_H
#define MACROS_H

#include "string.h"

#define NELEMS(array) (sizeof(array) / sizeof(array[0]))
#define ASSERT(test) _Static_assert(test, #test)
#define BIT_MASK(n) ((1 << n) - 1)
#define RUN(f, ...)  if(f){f(__VA_ARGS__ );}
#define RUN_AND_CLEAN_BEFORE(f, ...)  if(f){void (*tempF)(void) = f; f = NULL; tempF(__VA_ARGS__);}
#define PASS() {volatile uint8_t dummy = 0; (void)dummy;}
#define MAX_UINT8_T 0xFF

#define STD_CREATE_3BYTE_TABLE(value) {(value), (value) >> 8, (value) >> 16}
#define STD_CREATE_UINT32_FROM_TABLE3(value) (uint32_t)((value[0]) + ((uint32_t)value[1]) << 8, ((uint32_t)(value)) << 16)

static inline int MAX(int a, int b) {
  return (a > b) ? a : b;
}

static inline int MIN(int a, int b) {
  return (a < b) ? a : b;
}

static inline uint16_t Cast_toUint16(uint8_t *pointer)
{
    return pointer[0] | (pointer[1] << 8);
}

static inline uint8_t memcpySafe(void *destination, void *source, size_t size)
{
    if(destination != NULL && source != NULL && size != 0)
    {
        memcpy(destination, source, size);
        return 1;
    }
    return 0;
}

static inline uint32_t Cast_toUint32(uint8_t *pointer)
{
    return pointer[0] | (pointer[1] << 8) | (pointer[2] << 16) | (pointer[3] << 24);
}
#endif
