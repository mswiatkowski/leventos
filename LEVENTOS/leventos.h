#ifndef EVENT_EVENT_H_
#define EVENT_EVENT_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	void (*setNewTime)(int newTime);
	void (*turnOffTimer)(void);
	int (*getTimerCounter)(void);
	float (*getUsInOneTick)(void);
	int (*getOverlapTick)(void);
}Leventos_EventHardwareInterface_t;

typedef enum {
	Leventos_EventRemoveOne = (uint8_t)0x00,
	Leventos_EventRemoveAll = (uint8_t)0x01
}Leventos_EventRemove_t;

typedef struct {
	uint32_t s;
	uint32_t ms;
	uint32_t us;
}Leventos_Time_t;

#define LEVENTOS_US(x) ((Leventos_Time_t){.s = 0, .ms = 0, .us = x})
#define LEVENTOS_MS(x) ((Leventos_Time_t){.s = 0, .ms = x, .us = 0})
#define LEVENTOS_SEC(x) ((Leventos_Time_t){.s = x, .ms = 0, .us = 0})


typedef struct {
	void (*funct)(void);
	int32_t time;
}Leventos_Event_t;

void Leventos_init(Leventos_EventHardwareInterface_t interface);

void Leventos_delay(Leventos_Time_t time, void (*sleep)(void));

int Leventos_add(Leventos_Time_t time, void (*funct)(void));
int Leventos_delete(void (*funct)(void), Leventos_EventRemove_t remove);

void Leventos_irq(void);

void Leventos_handler(void);

/* Example of use:
 *
 * static void led_on(void);
 *
 * Leventos_add(LEVENTOS_MS(100), led_on);
 *
 * static void led_on(void)
 * {
 * 	LED1_ON();
 * }
 *
 * LED1 will be on after 100ms
 * 30us is minimum resolution of counter (1/32768)
 *
 * New task can be added in other task ex.
 *
 * static void task_1(void);
 * static void task_2(void);
 *
 * Leventos_add(LEVENTOS(100), task_1);
 *
 * static void task_1(void)
 * {
 * 	LED1_ON();
 * 	Leventos_add(LEVENTOS_MS(1000), task_2);
 * }
 *
 * static void task_2(void)
 * {
 * 	LED2_ON();
 * }
 *
 * LED1 will be on after 100ms from start, LED2 will be on after 1100ms
 *
 * Task can be execute continuously:
 *
 * static void task_continuously(void);
 *
 * task_continuously(); //in any init
 *
 * static void task_continuously(void)
 * {
 * 	LED1_TOOGLE();
 * 	Leventos_add(LEVENTOS_MS(500), task_continuously);
 * }
 *
 * LED will flash witch 2Hz frequency
 *
 *
 * Leventos_delay function wait for any amount of time, sleep argument is pointer to function that will be called while delay time.
 * If we don't need to call function in delay, we can pass NULL
 */

#endif
