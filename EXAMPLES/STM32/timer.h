#ifndef _TIMER_H_
#define _TIMER_H_

#define TIMER_US_IN_ONE_TICK 30.517578125
#define TIMER_NEAR_OVERLAP_VALUE 0xf000

void TIMER_init(void);

#endif
