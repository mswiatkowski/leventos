#include "timer.h"

#include "../LEVENTOS/leventos.h"

static void setNewTime(int value);
static void turnOffTimer(void);
static int getTimerCounter(void);
static float getUsInOneTick(void);
static int getOverlapTick(void);

void TIMER_init(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_TIM21EN;

    TIM21->CR1 |= TIM_CR1_URS;
    TIM21->PSC = 979;
    TIM21->ARR = 32768 - 1;
    TIM21->CCER |= TIM_CCER_CC1E;
    TIM21->EGR |= TIM_EGR_UG;
    TIM21->CR1 |= TIM_CR1_CEN;

    NVIC_EnableIRQ(TIM21_IRQn);

    TIMER_Event_init((TIMER_EventHardwareInterface_t){
        .setNewTime = setNewTime,
        .turnOffTimer = turnOffTimer,
        .getTimerCounter = getTimerCounter,
        .getUsInOneTick = getUsInOneTick,
        .getOverlapTick = getOverlapTick,
        });
}

void TIM21_IRQHandler(void)
{
    if((TIM21->SR & TIM_SR_CC1IF) && (TIM21->DIER & TIM_DIER_CC1IE))
    {
        Leventos_irq();
        TIM21->SR &= ~TIM_SR_CC1IF;
    }
}

static void setNewTime(int value)
{
    TIM21->CCR1 = value;
    TIM21->DIER |= TIM_DIER_CC1IE;
    TIM21->SR &= ~TIM_SR_CC1IF;
}

static void turnOffTimer(void)
{
    TIM21->DIER &= ~TIM_DIER_CC1IE;
}

static int getTimerCounter(void)
{
    return TIM21->CNT;
}

static float getUsInOneTick(void)
{
    return TIMER_US_IN_ONE_TICK;
}

static int getOverlapTick(void)
{
    return TIMER_NEAR_OVERLAP_VALUE;
}
