#ifndef _RTC_H_
#define _RTC_H_

#define RTC_US_IN_ONE_TICK        30.517578125
#define RTC_NEAR_OVERLAP_VALUE    0xf000

void RTC_init(void);

#endif
