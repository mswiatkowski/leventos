#include "rtc.h"

#include "../LEVENTOS/leventos.h"

#include "em_cmu.h"
#include "em_emu.h"
#include "em_rtc.h"

static void setNewTime(int newTime);
static void turnOffTimer(void);
static int getTimerCounter(void);
static float getUsInOneTick(void);
static int getOverlapTick(void);

void RTC_init(void)
{ 
    CMU->OSCENCMD = CMU_OSCENCMD_LFXOEN; 
    while(!(CMU->STATUS & CMU_STATUS_LFXORDY));

    EMU_UpdateOscConfig();
    CMU->LFCLKSEL = (CMU->LFCLKSEL & (~(_CMU_LFCLKSEL_LFA_MASK
                                        | _CMU_LFCLKSEL_LFAE_MASK) |
                                    (_CMU_LFCLKSEL_LFA_LFXO << _CMU_LFCLKSEL_LFA_SHIFT)
                                        | (0 << _CMU_LFCLKSEL_LFAE_SHIFT)));
    CMU_ClockEnable(cmuClock_RTC, true);
    CMU_ClockEnable(cmuClock_CORELE, true);

    RTC->COMP0 = 0xffff;
    RTC->COMP1 = 0xffff;

    RTC_IntClear(RTC_IFC_COMP1);
    RTC_IntEnable(RTC_IEN_COMP1);
    NVIC_EnableIRQ(RTC_IRQn);

    RTC->CTRL = RTC_CTRL_EN | RTC_CTRL_COMP0TOP;

    TIMER_Event_init((TIMER_EventHardwareInterface_t){
        .setNewTime = setNewTime,
        .turnOffTimer = turnOffTimer,
        .getTimerCounter = getTimerCounter,
        .getUsInOneTick = getUsInOneTick,
        .getOverlapTick = getOverlapTick,
    });
}

void RTC_IRQHandler(void)
{
    if((RTC->IEN & RTC_IEN_COMP1) && (RTC->IF & RTC_IF_COMP1))
    {
        Leventos_irq();
        RTC->IFC = RTC_IFC_COMP1;
    }
}

static void setNewTime(int value)
{
    RTC->CNT = 0;
    RTC->COMP1 = value;
    RTC->IFC = RTC_IFC_COMP1;
    RTC_IntEnable(RTC_IEN_COMP1);
}

static void turnOffTimer(void)
{
    RTC_IntDisable(RTC_IEN_COMP1);
}

static int getTimerCounter(void)
{
    return RTC->CNT;
}

static float getUsInOneTick(void)
{
    return RTC_US_IN_ONE_TICK;
}

static int getOverlapTick(void)
{
    return RTC_NEAR_OVERLAP_VALUE;
}
